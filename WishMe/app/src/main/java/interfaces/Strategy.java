package interfaces;

/**
 * Created by Jobayed Ullah on 11/3/2017.
 */

public interface Strategy
{
    void init();
}
