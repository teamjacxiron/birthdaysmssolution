package interfaces;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import database.Birthdays;
import database.Singleton;
import observer.MessageObserver;
import observer.ObserverNode;
import softweb.wishme.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Jobayed Ullah on 11/3/2017.
 */

interface State
{
    void changeState(Context mContext,DateChange context);
}

class UnChanged implements State
{
    @Override
    public void changeState(Context mContext,DateChange context)
    {

        if(context.isDateChanged(context.day,context.month)==true)
        {
            context.setCurrentState(new Changed());
            context.getCurrentState().changeState(mContext,context);
        }
        else Toast.makeText(mContext, "Date not Changed", Toast.LENGTH_LONG).show();
    }


}

class Changed implements State
{
    Singleton singleton = Singleton.getInstance();
    Context   mContext;
    @Override
    public void changeState(Context mContext,DateChange context)
    {
        this.mContext = mContext;
        task();
        context.setCurrentState(new UnChanged());
    }

    private void task()
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = dateFormat.format(calendar.getTime());

        int month = Integer.parseInt(date.split("/")[1]);
        int day   = Integer.parseInt(date.split("/")[2]);

        //unset the sent value to 0
        singleton.updateLogTableSent(mContext);

        ArrayList<Birthdays> p = new ArrayList<Birthdays>();
        p = singleton.getTodayBirthdays(mContext, day + "", month + "");

        MessageObserver messageObserver = new MessageObserver(mContext);
        ArrayList<ObserverNode> observerNodes = new ArrayList<ObserverNode>();
        String msg = "";

        Toast.makeText(mContext,"Date Changed: ",Toast.LENGTH_SHORT).show();
        for (int i = 0; i < p.size(); i++)
        {
            try {
                msg = "Today is your "+p.get(i).getRelaion()+' '+p.get(i).getName()+"'s"+"Birthday.";
                NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
                //PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intentNotification,0);
                //Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notificationsound);
                //.setContentIntent(contentIntent)
                Notification notification = new Notification.Builder(mContext)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Wish: "+p.get(i).getName())
                        .setStyle(new Notification.BigTextStyle().bigText(msg))
                        .setAutoCancel(true)
                        .setContentText(msg)
                        .build();
                notificationManager.notify(0, notification);
            }
            catch (Exception e)
            {
                Toast.makeText(mContext,"Problem"+e.getMessage(),Toast.LENGTH_LONG).show();
            }

            try {
                ContentValues val = new ContentValues();
                val.put("value", "ID_ZERO");
                mContext.getContentResolver().update(Uri.parse("content://settings/system"), val, "name='sms_sim_setting'", null);

                String phoneNumberReciver="+8801"+p.get(i).getPhone().substring(2,11);// phone number to which SMS to be send
                String message="Happy Birthday, "+p.get(i).getName()
                        +". Wish you many many happy returns of the day.";// message to send
                //SmsManager sms = SmsManager.getDefault();
                //sms.sendTextMessage(phoneNumberReciver, null, message, null, null);

                //Toast.makeText(getBaseContext(), "SMS not delivered",Toast.LENGTH_SHORT).show();

                observerNodes.add(new ObserverNode(mContext,p.get(i),messageObserver));
                //observerNodes.get(i).messageObserver.sendSMS(phoneNumberReciver,message,i);
                //sendSMS(phoneNumberReciver,message);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        //set log table to current date on sms sent success
        singleton.updateLogTable(mContext,day,month);



    }





}


public class DateChange
{

    public Context mContext;
    private State currentState;
    Singleton singleton = Singleton.getInstance();
    int month;
    int day;

    public State getCurrentState()
    {
        return currentState;
    }

    public void setCurrentState(State currentState)
    {
        this.currentState = currentState;
    }



    /// Constructor
    public DateChange(Context context)
    {
        this.mContext      = context;
        this.currentState = new UnChanged();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = dateFormat.format(calendar.getTime());

        this.month = Integer.parseInt(date.split("/")[1]);
        this.day   = Integer.parseInt(date.split("/")[2]);
    }

    public void changeState()
    {
        this.currentState.changeState(mContext,this);
    }

    public boolean isDateChanged(int day,int month)
    {
        int[] logs = new int[3];
        logs = singleton.getAll_Logs(mContext);

        Toast.makeText(mContext,"log : "+ logs[0]+' '+logs[1]+'\n', Toast.LENGTH_LONG).show();

        if(logs[2]==0)return true;


        if(!(month == logs[1] && day==logs[0]))return true;
        return false;
    }

    /*public boolean isDateChanged(int day,int month)
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("h:m");
        String date = dateFormat.format(calendar.getTime());

        //this.month = Integer.parseInt(date.split("/")[1]);
        //this.day   = Integer.parseInt(date.split("/")[2]);

        int h = Integer.parseInt(date.split(":")[0]);
        int m = Integer.parseInt(date.split(":")[1]);

        if(m>1 && m<59)return true;
        return false;
    }*/
}
