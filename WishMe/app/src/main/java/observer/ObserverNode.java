package observer;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import database.Birthdays;
import softweb.wishme.R;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Jobayed Ullah on 11/3/2017.
 */

public class ObserverNode extends Observer
{
    Birthdays birthday;
    Context mContext;

    public ObserverNode(Context context,Birthdays aBirthday,MessageObserver messageObserver)
    {
        this.mContext = context;
        this.birthday = aBirthday;
        this.messageObserver = messageObserver;
        this.messageObserver.attach(this);
    }

    @Override
    public void update()
    {
        String msg = "Wish Sent to "+birthday.getName()+" in "+(birthday.getGender().equalsIgnoreCase("male")?"his":"her" + " Birthday.");

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
        //PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intentNotification,0);
        //Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.notificationsound);
        //.setContentIntent(contentIntent)
        Notification notification = new Notification.Builder(mContext)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Wish Sent: "+birthday.getName())
                .setStyle(new Notification.BigTextStyle().bigText(msg))
                .setAutoCancel(true)
                .setContentText(msg)
                .build();
        notificationManager.notify(0, notification);
    }
}
