package observer;

/**
 * Created by Jobayed Ullah on 11/3/2017.
 */

public abstract class Observer
{
    public MessageObserver messageObserver;
    public abstract void update();
}
