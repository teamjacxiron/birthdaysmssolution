package observer;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jobayed Ullah on 11/3/2017.
 */

public class MessageObserver
{
    Context mContext;

    private List<Observer> observers = new ArrayList<Observer>();
    private int index;

    public MessageObserver(Context mContext)
    {
        this.mContext = mContext;
    }


    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
        notifyObserver();
    }

    public void attach(Observer observer)
    {
        observers.add(observer);
    }

    private void notifyObserver()
    {

        observers.get(index).update();
    }

    /**
     * ---sends an SMS message to another device---
     *
     * @param phoneNumber
     * @param message
     */
    public void sendSMS(final String phoneNumber, final String message,final int i)
    {
        // Intent Filter Tags for SMS SEND and DELIVER
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        // STEP-1___
        // SEND PendingIntent
        PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0, new Intent(SENT), 0);

        // DELIVER PendingIntent
        PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0,
                new Intent(DELIVERED), 0);
        // STEP-2___
        // SEND BroadcastReceiver



        final BroadcastReceiver sendSMS = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context arg0, Intent arg1)
            {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(mContext, "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        setIndex(i);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(mContext, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        //sendSMS(phoneNumber,message);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(mContext, "No service",
                                Toast.LENGTH_SHORT).show();
                        //sendSMS(phoneNumber,message);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(mContext, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        //sendSMS(phoneNumber,message);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(mContext, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        //sendSMS(phoneNumber,message);
                        break;
                }
            }
        };

        // DELIVERY BroadcastReceiver
        BroadcastReceiver deliverSMS = new BroadcastReceiver()
        {
            @Override
            public void onReceive(Context arg0, Intent arg1)
            {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(mContext, "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(mContext, "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        // STEP-3___
        // ---Notify when the SMS has been sent---
        mContext.registerReceiver(sendSMS, new IntentFilter(SENT));

        // ---Notify when the SMS has been delivered---
        mContext.registerReceiver(deliverSMS, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }
}
