package adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import database.DateListedBirthdays;
import database.Singleton;
import softweb.wishme.R;

import static android.widget.GridLayout.VERTICAL;

/**
 * Created by Jobayed Ullah on 1/10/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolders>
{
    private ArrayList<DateListedBirthdays> mItems;
    Context mContext;
    Singleton s = Singleton.getInstance();
    Typeface light;


    // Disable touch detection for parent recyclerView if we use vertical nested recyclerViews
    private View.OnTouchListener mTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            v.getParent().requestDisallowInterceptTouchEvent(true);
            return false;
        }
    };

    public CustomAdapter(Context context)
    {
        this.mContext = context;
        mItems        = new ArrayList<>();
        light         = s.getFontFromRes(mContext,R.raw.rslight);
    }

    public void addItems(DateListedBirthdays items) {
        mItems.add(items);
    }



    @Override
    public CustomViewHolders onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_snap, parent, false);

        if (viewType == VERTICAL)
        {
            view.findViewById(R.id.recyclerView).setOnTouchListener(mTouchListener);
        }

        return new CustomViewHolders(view);

    }

    @Override
    public void onBindViewHolder(CustomViewHolders holder, int position)
    {
        DateListedBirthdays itm = mItems.get(position);
        if(position == 0)
        {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            String date = dateFormat.format(calendar.getTime());
            int month = Integer.parseInt(date.split("/")[1]);
            int day   = Integer.parseInt(date.split("/")[2]);

            if(Integer.parseInt(itm.getI().get(0).getDay())==day && Integer.parseInt(itm.getI().get(0).getMonth())==(month))
                holder.snapTextView.setText("TODAY");
            else holder.snapTextView.setText(itm.getStr());
        }
        else
        holder.snapTextView.setText(itm.getStr());
        holder.snapTextView.setTypeface(light);

        if(position==0)
        holder.recyclerView.setLayoutManager(
                new LinearLayoutManager(holder.recyclerView.getContext(), LinearLayoutManager.HORIZONTAL, false));

        else holder.recyclerView.setLayoutManager(
                new LinearLayoutManager(holder.recyclerView.getContext(), LinearLayoutManager.VERTICAL, false));
        holder.recyclerView.setOnFlingListener(null);
            //new GravitySnapHelper(snap.getGravity(), false, this).attachToRecyclerView(holder.recyclerView);



        holder.recyclerView.setAdapter(new RecyclerViewAdapter(mContext, mItems.get(position).getI() , position) );
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }






    public static class CustomViewHolders extends RecyclerView.ViewHolder
    {

        public TextView snapTextView;
        public RecyclerView recyclerView;

        public CustomViewHolders(View itemView) {
            super(itemView);
            snapTextView = (TextView) itemView.findViewById(R.id.snapTextView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerView);
        }


    }
}
