package adapters;

/**
 * Created by Jobayed Ullah on 1/9/2017.
 */

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import database.Birthdays;
import database.Singleton;
import softweb.wishme.R;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolders>
{

    private List<Birthdays> itemList;
    private int dCount;
    Context mContext;

    Singleton s = Singleton.getInstance();
    Typeface digital,bold,light,thin;

    public RecyclerViewAdapter(Context context,List<Birthdays> itemList, int dCount)
    {
        this.itemList = itemList;
        this.dCount   = dCount;
        this.mContext = context;

        digital = s.getFontFromRes(mContext,R.raw.digital);
        bold = s.getFontFromRes(mContext,R.raw.rsbold);
        light = s.getFontFromRes(mContext,R.raw.rslight);
        thin = s.getFontFromRes(mContext,R.raw.rsthin);
    }


    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View layoutView;
        if(dCount == 0)layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.today, null);
        else layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.other_day, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position)
    {
        //Name
        holder.name.setText(itemList.get(position).getName());
        holder.name.setTypeface(light);
        //Year
        if(dCount == 0)
        holder.tvYear.setText(itemList.get(position).getYear().substring(2,4));
        else holder.tvYear.setText(itemList.get(position).getYear());
        holder.tvYear.setTypeface(thin);
        //image
        try
        {
            Bitmap b = getContactBitmapFromURI(mContext,fetchPhotoURIFromPhoneNumber(itemList.get(position).getPhone(),mContext));
            if(b!=null)
            holder.image.setImageBitmap(b);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public static class RecyclerViewHolders extends RecyclerView.ViewHolder
    {

        public TextView name;
        public ImageView image;
        public TextView tvYear;

        public RecyclerViewHolders(View itemView)
        {
            super(itemView);


                name = (TextView)itemView.findViewById(R.id.name);
                image = (ImageView)itemView.findViewById(R.id.image_icon);
                tvYear       = (TextView)itemView.findViewById(R.id.txt_year);


        }


    }






    public Uri fetchPhotoURIFromPhoneNumber(String phoneNumber, Context context)
    {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(phoneNumber));
        Cursor cursor = context.getContentResolver().query(uri,
                new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID },
                null, null, null);

        String contactId = "";

        if (cursor.moveToFirst()) {
            do {
                contactId = cursor.getString(cursor
                        .getColumnIndex(ContactsContract.PhoneLookup._ID));
            } while (cursor.moveToNext());
        }

        return getPhotoURIFromId(contactId,context);
    }

    private Uri getPhotoURIFromId(String id,Context context)
    {

        try {
            Cursor cursor = context.getContentResolver().query(
                    ContactsContract.Data.CONTENT_URI,
                    null,
                    ContactsContract.Data.CONTACT_ID + "="
                            + id
                            + " AND "
                            + ContactsContract.Data.MIMETYPE
                            + "='"
                            + ContactsContract.CommonDataKinds.Photo.CONTENT_ITEM_TYPE
                            + "'",
                    null,
                    null
            );

            if (cursor != null) {
                if (!cursor.moveToFirst()) {
                    return null; // no photo
                }
            } else {
                return null; // error in cursor process
            }
            cursor.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        Uri person = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.valueOf(id));
        return Uri.withAppendedPath(person, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
    }

    public static Bitmap getContactBitmapFromURI(Context context, Uri uri)
    {
        InputStream input = null;
        try {
            input = context.getContentResolver().openInputStream(uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (input == null)
        {
            return null;
        }
        return BitmapFactory.decodeStream(input);
    }


}