package services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServiceBroadCastReceiver extends BroadcastReceiver
{
    public ServiceBroadCastReceiver() {}

    @Override
    public void onReceive(Context context, Intent intent)
    {
        context.stopService(new Intent(context,TimeCheckService.class));
        Intent i = new Intent(context, TimeCheckService.class);
        context.startService(i);
    }


}
