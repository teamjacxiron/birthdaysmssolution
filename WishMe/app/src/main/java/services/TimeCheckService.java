package services;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import database.Birthdays;
import database.Singleton;
import interfaces.DateChange;
import softweb.wishme.R;

import static com.codetroopers.betterpickers.timezonepicker.TimeZoneFilterTypeAdapter.TAG;

public class TimeCheckService extends Service
{

    public Context context = this;
    public Handler handler = null;
    public static Runnable runnable = null;
    Singleton singleton = Singleton.getInstance();


    @Override
    public IBinder onBind(Intent intent) {return null;}



    @Override
    public void onCreate()
    {
        //Toast.makeText(this, "Service created!", Toast.LENGTH_LONG).show();

        handler = new Handler();
        runnable = new Runnable()
        {
            public void run()
            {
                serviceTask();

                //20s
                handler.postDelayed(runnable, 1000*20); //1000ms*60s*60m*6h = 21600000ms = 21600s = 360min = 6h
            }
        };
        handler.postDelayed(runnable, 60000);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////SERVICE ACTIVITY GOES HERE////////////////////////////////////
    ///////////////////////////////////////////the code before and after/////////////////////////////////////
    ///////////////////////////////////////////service activity should///////////////////////////////////////
    ///////////////////////////////////////////NOT BE EDITED***//////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    private void serviceTask()
    {

        DateChange dateChange = new DateChange(this.context);
        dateChange.changeState();
    }





    //Still not used
    /*
    public static void getDevId(Context context)
    {
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        try
        {

            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getFirstMethod = telephonyClass.getMethod("getDefault", parameter);

            Log.d("tag", getFirstMethod.toString());

            Object[] obParameter = new Object[1];
            obParameter[0] = 0;
            TelephonyManager first = (TelephonyManager) getFirstMethod.invoke(null, obParameter);

            Log.d("TAG", "Device Id: " + first.getDeviceId() + ", device status: " + first.getSimState() + ", operator: " + first.getNetworkOperator() + "/" + first.getNetworkOperatorName());

            obParameter[0] = 1;
            TelephonyManager second = (TelephonyManager) getFirstMethod.invoke(null, obParameter);

            Log.d("TAG", "Device Id: " + second.getDeviceId() + ", device status: " + second.getSimState()+ ", operator: " + second.getNetworkOperator() + "/" + second.getNetworkOperatorName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////SERVICE ACTIVITY ENDS HERE/////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        //Toast.makeText(this, "Service started by user.", Toast.LENGTH_LONG).show();
        return super.onStartCommand(intent, flags, startId);

    }




    @Override
    public void onDestroy()
    {
        super.onDestroy();
        /* IF YOU WANT THIS SERVICE KILLED WITH THE APP THEN UNCOMMENT THE FOLLOWING LINE */
        handler.removeCallbacks(runnable);
        //Toast.makeText(this, "Service stopped", Toast.LENGTH_LONG).show();

    }


    public void broadcastIntent()
    {
        Intent intent = new Intent();
        intent.setAction("com.exampled.beta.CUSTOM_INTENT");
        sendBroadcast(intent);
        //Toast.makeText(getApplicationContext(), "BroadCaste", Toast.LENGTH_LONG).show();

    }
}





