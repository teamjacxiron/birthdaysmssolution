package database;

import java.util.List;

/**
 * Created by Jobayed Ullah on 1/10/2017.
 */

public class DateListedBirthdays
{

    private String str;
    private List<Birthdays> i;


    public DateListedBirthdays(String str, List<Birthdays> i)
    {
        this.str = str;
        this.i = i;
    }





    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public List<Birthdays> getI() {
        return i;
    }

    public void setI(List<Birthdays> i) {
        this.i = i;
    }



}
