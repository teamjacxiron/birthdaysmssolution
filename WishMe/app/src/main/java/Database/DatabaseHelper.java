package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by Jobayed Ullah on 1/4/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "wish_me";
    public static final int databaseVersion = 3;

    public static final String TABLE_BIRTHDAY = "birthday";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DAY = "day";
    public static final String MONTH = "month";
    public static final String YEAR = "year";
    public static final String RELATION = "relation";
    public static final String GENDER = "gender";
    public static final String PHONE = "phone";


    public static final String TABLE_LOG = "log";

    public static final String LOG_FIELD  = "log_field";
    public static final String LOG_VALUE  = "log_value";

    private HashMap hp;


    public String createTableBirthdayQuery ="create table "+ TABLE_BIRTHDAY+"(" +
            ID + " int primary key,"+
            NAME + " text,"+
            DAY + " text,"+
            MONTH + " text,"+
            YEAR + " text,"+
            RELATION+" text,"+
            GENDER+" text,"+
            PHONE+" text);";

    public String createTableLogQuery ="create table "+ TABLE_LOG+"(" +
            LOG_FIELD + " text primary key,"+
            LOG_VALUE+" text);";

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME , null, databaseVersion);
        Log.d("database operation",DATABASE_NAME+" Database Created");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        sqLiteDatabase.execSQL(createTableBirthdayQuery);
        sqLiteDatabase.execSQL(createTableLogQuery);

        //
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = dateFormat.format(calendar.getTime());

        String month = Integer.parseInt(date.split("/")[1])+"";
        String day   = Integer.parseInt(date.split("/")[2])+"";

        String query="INSERT INTO "+TABLE_LOG+" VALUES ( ? , ?)";

        //
        sqLiteDatabase.execSQL(query, new String[]{"day",day});
        sqLiteDatabase.execSQL(query, new String[]{"month",month});
        sqLiteDatabase.execSQL(query, new String[]{"sent","0"});

        Log.d("database operation","Birthday & LOG Table Created");
    }

    /*@Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }*/

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_BIRTHDAY);
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_LOG);
        onCreate(db);
    }

    public void insertAbirthday (   String name,
                                    String day,
                                    String month,
                                    String year,
                                    String relation,
                                    String gender,
                                    String phone)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("day", day);
        contentValues.put("month", month);
        contentValues.put("year", year);
        contentValues.put("relation", relation);
        contentValues.put("gender",gender);
        contentValues.put("phone",phone);
        long k = db.insert(TABLE_BIRTHDAY, null, contentValues);
        if(k!=0)
        Log.d("database operation","Data inserted");
    }


    public void updateAlog (Context context, String wh, String val)
    {
        final ContentValues values = new ContentValues();
        values.put(LOG_VALUE, val);
        try
        {
            SQLiteDatabase database = this.getWritableDatabase();
            String updateQuery = LOG_FIELD + "=" + "'"+wh+"'";
            database.update(TABLE_LOG, values, updateQuery, null);
        } catch (SQLException e) {
            Toast.makeText(context,e.getMessage(),Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * this is a raw database query which returns all the rows of the table "productTableName"
     * Added by "Jobayed Ullah"
     * this method returns a cursor
     */
    public Cursor getAllLogs(DatabaseHelper databaseHelper)
    {
        String query = "SELECT * FROM "+ TABLE_LOG;
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        Cursor  cursor = database.rawQuery(query,null);
        return  cursor;
    }

    /*
     * this is a raw database query which returns all the rows of the table "productTableName"
     * Added by "Jobayed Ullah"
     * this method returns a cursor
     */
    public Cursor getAllBirthdays(DatabaseHelper databaseHelper)
    {
        String query = "SELECT * FROM "+ TABLE_BIRTHDAY;
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        Cursor  cursor = database.rawQuery(query,null);
        return  cursor;
    }

    /*
     * this is a raw database query which returns all the rows of the table "productTableName"
     * WHERE month and day are some specific value
     * Added by "Jobayed Ullah"
     * this method returns a cursor
     */

    public Cursor getAllBirthdaysWhereEquals(DatabaseHelper databaseHelper,String day,String month)
    {
        String query = "SELECT * FROM '"+ TABLE_BIRTHDAY +"' WHERE "+
                       DAY +" = '" +day+"' AND "+ MONTH +" = '"+month+"'";
        SQLiteDatabase database = databaseHelper.getWritableDatabase();
        Cursor  cursor = database.rawQuery(query,null);
        return  cursor;
    }




}
