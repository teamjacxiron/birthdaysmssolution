package database;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

/**
 * Created by Jahid on 11/1/2016.
 */

public class Singleton
{






    /*create an object of SingleObject*/
    private static Singleton instance = new Singleton();

    private Singleton(){}

    /*Get the only object available*/
    public static Singleton getInstance()
    {
        return instance;
    }



    public void insertAbirthday (   Context context,
                                    String name,
                                    String day,
                                    String month,
                                    String year,
                                    String relation,
                                    String gender,
                                    String phone)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        databaseHelper.insertAbirthday(name,day, month, year, relation, gender,phone);
    }


    /*
     * Added by Jobayed Ullah on 09/11/2016
     *
     * This public method is to populate Log table data in a String
     * it takes an object of type Context as argument
     *
     */
    public int[] getAll_Logs(Context context)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Cursor cursor = databaseHelper.getAllLogs(databaseHelper);

        int[] p = new int[3];
        if (cursor.getCount()>0)
        {
            int i=0;
            while (cursor.moveToNext())
            {
                //String tmp = cursor.getString(cursor.getColumnIndex(databaseHelper.LOG_FIELD));
                //if(tmp.equals("day"))
                    p[i++] = Integer.parseInt( cursor.getString(cursor.getColumnIndex(databaseHelper.LOG_VALUE)) );
                //else if(tmp.equals("month"))
                    //p[1] = Integer.parseInt( cursor.getString(cursor.getColumnIndex(databaseHelper.LOG_VALUE)) );
                //else if(tmp.equals("sent"))
                    //p[2] = Integer.parseInt( cursor.getString(cursor.getColumnIndex(databaseHelper.LOG_VALUE)) );
            }
        }


        return p;
    }

    public void updateLogTableSent(Context context)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        databaseHelper.updateAlog(context,"sent","0");
    }

    public void updateLogTable(Context context,int day,int month)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        databaseHelper.updateAlog(context,"day",day+"");
        databaseHelper.updateAlog(context,"month",month+"");
        databaseHelper.updateAlog(context,"sent","1");
    }

    /*
     * Added by Jobayed Ullah on 09/11/2016
     *
     * This public method is to populate Birthdays table data in an Arraylist
     * it takes an object of type 'Birthdays' class as argument
     * and returns an Arraylist.
     */
    public ArrayList<Birthdays> getAllBirthdays(Context context)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Cursor cursor = databaseHelper.getAllBirthdays(databaseHelper);

        ArrayList<Birthdays> p = new ArrayList<Birthdays>();
        if (cursor.getCount()>0)
        {
            while (cursor.moveToNext())
            {
                Birthdays birthdays = new Birthdays
                    (cursor.getString(cursor.getColumnIndex(databaseHelper.NAME)),
                            cursor.getString(cursor.getColumnIndex(databaseHelper.DAY)),
                            cursor.getString(cursor.getColumnIndex(databaseHelper.MONTH)),
                            cursor.getString(cursor.getColumnIndex(databaseHelper.YEAR)),
                            cursor.getString(cursor.getColumnIndex(databaseHelper.RELATION)),
                            cursor.getString(cursor.getColumnIndex(databaseHelper.GENDER)),
                            cursor.getString(cursor.getColumnIndex(databaseHelper.PHONE))
                    );
                p.add(birthdays);
            }
        }

        Collections.sort(p);
        return p;
    }

    public ArrayList<Birthdays> getAllBirthdaysEquipped(Context context)
    {
        ArrayList<Birthdays> allBirthdaysSorted = this.getAllBirthdays(context);

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = dateFormat.format(calendar.getTime());

        int tMonth = Integer.parseInt(date.split("/")[1]);
        int tDay   = Integer.parseInt(date.split("/")[2]);
        int m,d;

        int len = allBirthdaysSorted.size();
        ArrayList<Birthdays> allBirthdaysEquipped = new ArrayList<Birthdays>();
        int i = 0,x=0;

        for (; i<len; i++)
        {
            m = Integer.parseInt(allBirthdaysSorted.get(i).getMonth());
            d = Integer.parseInt(allBirthdaysSorted.get(i).getDay());

            if(m == tMonth)
            {
                if(d<tDay)continue;
                else break;
            }
            else if(m>tMonth)break;
        }

        //Toast.makeText(context,i+"",Toast.LENGTH_SHORT).show();


        x = i;
        for (; i < len; i++)
        {
                allBirthdaysEquipped.add(allBirthdaysSorted.get(i));
        }
        for (i=0; i < x; i++)
        {
            allBirthdaysEquipped.add(allBirthdaysSorted.get(i));
        }





        return allBirthdaysEquipped;
    }


    public ArrayList<ArrayList<Birthdays>> getAllBirthdaysDatewise(Context context)
    {
        ArrayList<Birthdays> allBirthdaysEquipped = this.getAllBirthdaysEquipped(context);
        ArrayList<ArrayList<Birthdays>> dateWise  = new ArrayList<ArrayList<Birthdays>>();
        int len = allBirthdaysEquipped.size();

        ArrayList<Birthdays> tmp = new ArrayList<Birthdays>();
        if(len>0)
        {
            int m = Integer.parseInt(allBirthdaysEquipped.get(0).getMonth());
            int d = Integer.parseInt(allBirthdaysEquipped.get(0).getDay());
            int m2,d2;
            for(int i=0;i<len;i++)
            {
                m2 = Integer.parseInt(allBirthdaysEquipped.get(i).getMonth());
                d2 = Integer.parseInt(allBirthdaysEquipped.get(i).getDay());

                if(m==m2 && d==d2)
                {
                    tmp.add(allBirthdaysEquipped.get(i));

                }
                else
                {
                    dateWise.add(new ArrayList<Birthdays>(tmp));
                    tmp.clear();
                    tmp.add(allBirthdaysEquipped.get(i));

                    m = m2;
                    d = d2;
                }
            }
        }

        dateWise.add(new ArrayList<Birthdays>(tmp));

        return dateWise;

    }


    /*
     * Added by Jobayed Ullah on 09/11/2016
     *
     * This public method is to populate TODAY Birthdays table data in an Arraylist
     * it takes an object of type 'Birthdays' class as argument
     * and returns an Arraylist.
     */
    public ArrayList<Birthdays> getTodayBirthdays(Context context,String day,String month)
    {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        Cursor cursor = databaseHelper.getAllBirthdaysWhereEquals(databaseHelper,day,month);

        ArrayList<Birthdays> p = new ArrayList<Birthdays>();
        if (cursor.getCount()>0)
        {
            while (cursor.moveToNext())
            {
                Birthdays birthdays = new Birthdays
                        (cursor.getString(cursor.getColumnIndex(databaseHelper.NAME)),
                                cursor.getString(cursor.getColumnIndex(databaseHelper.DAY)),
                                cursor.getString(cursor.getColumnIndex(databaseHelper.MONTH)),
                                cursor.getString(cursor.getColumnIndex(databaseHelper.YEAR)),
                                cursor.getString(cursor.getColumnIndex(databaseHelper.RELATION)),
                                cursor.getString(cursor.getColumnIndex(databaseHelper.GENDER)),
                                cursor.getString(cursor.getColumnIndex(databaseHelper.PHONE))
                        );
                p.add(birthdays);
            }
        }

        Collections.sort(p);
        return p;
    }



    /*
     * this function adds a typeface from the raw resource .ttf file
     * Paremeter 2( context of the activity, resource identifier)
     */
    public Typeface getFontFromRes(Context context,int resource)
    {
        Typeface tf = null;
        InputStream is = null;
        try {
            is = context.getResources().openRawResource(resource);
        }
        catch(Resources.NotFoundException e) {
            Log.e("TAG:", "Could not find font in resources!");
        }

        String outPath = context.getCacheDir() + "/tmp" + System.currentTimeMillis() +".raw";

        try
        {
            byte[] buffer = new byte[is.available()];
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));

            int l = 0;
            while((l = is.read(buffer)) > 0)
                bos.write(buffer, 0, l);

            bos.close();

            tf = Typeface.createFromFile(outPath);

            // clean up
            new File(outPath).delete();
        }
        catch (IOException e)
        {
            Log.e("TAG", "Error reading in font!");
            return null;
        }

        Log.d("TAG", "Successfully loaded font.");

        return tf;
    }
}
