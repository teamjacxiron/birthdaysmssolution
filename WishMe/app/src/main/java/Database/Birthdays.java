package database;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Jobayed Ullah on 1/4/2017.
 */

//https://www.mkyong.com/android/how-to-send-sms-message-in-android/
//http://www.learn-android-easily.com/2013/06/scheduling-task-using-alarm-manager.html

public class Birthdays implements Comparable<Birthdays>
{


    String name;
    String day;
    String month;
    String year;
    String relation;
    String gender;
    String phone;

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public String getDay() {
        return day;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }

    public String getRelaion() {
        return relation;
    }

    public String getGender() {
        return gender;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setRelaion(String relaion) {
        this.relation = relation;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Birthdays()
    {
        this.name = null;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        String date = dateFormat.format(calendar.getTime());

        this.year  = Integer.parseInt(date.split("/")[0])+"";
        this.month = Integer.parseInt(date.split("/")[1])+"";
        this.day   = Integer.parseInt(date.split("/")[2])+"";

        this.relation = "Friend";
        this.gender  = "Male";

        this.phone = null;
    }

    public Birthdays(String name,
              String day,
              String month,
              String year,
              String relaion,
              String gender,
              String phone)
    {
        this.name     = name;
        this.day      =  day;
        this.month    = month;
        this.year     = year;
        this.relation  = relation;
        this.gender   = gender;
        this.phone    = phone;

    }

    public Birthdays(Birthdays b)
    {
        this.name     = b.name;
        this.day      = b.day;
        this.month    = b.month;
        this.year     = b.year;
        this.relation  = b.relation;
        this.gender   = b.gender;
        this.phone    = b.phone;

    }


    @Override
    public int compareTo(Birthdays compareBirthdays)
    {
        int thisMonth = Integer.parseInt(this.month);
        int templateMonth = Integer.parseInt(compareBirthdays.getMonth());

        if(thisMonth == templateMonth)
        {
            int thisDay = Integer.parseInt(this.day);
            int templateDay = Integer.parseInt(compareBirthdays.getDay());

            return (thisDay-templateDay);
        }
        else return (thisMonth - templateMonth);
    }
}
