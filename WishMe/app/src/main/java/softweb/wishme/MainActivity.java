package softweb.wishme;

import adapters.CustomAdapter;
import database.Birthdays;
import database.DateListedBirthdays;
import database.Singleton;
import interfaces.Strategy;
import services.TimeCheckService;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Strategy
{
    Singleton s = Singleton.getInstance();
    private RecyclerView mRecyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Typeface regular                = s.getFontFromRes(this,R.raw.rsregular);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this,R.color.colorPrimaryText));
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);

        /* Starting  TimeCheckService */
        Intent iTimeCheckService = new Intent(this, TimeCheckService.class);
        startService(iTimeCheckService);
        init();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(fabOnClickListener);
    }


    protected void onResume()
    {
        super.onResume();
        init();
    }

    public void init()
    {


        mRecyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        mRecyclerView.setHasFixedSize(true);

        setupAdapter();
    }

    View.OnClickListener fabOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view){

            Intent intent = new Intent(getApplicationContext(),AddNewBirthdayActivity.class);
            startActivity(intent);

        }
    };

    private void setupAdapter()
    {
        ArrayList<ArrayList<Birthdays>> allBirthdaysEquipped =  s.getAllBirthdaysDatewise(this);
        adapters.CustomAdapter customAdapter = new CustomAdapter(this);
        String[] months = {
                "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec"
        };

        for(ArrayList<Birthdays> item: allBirthdaysEquipped)
        {
            if(item.size()>0)
            customAdapter.addItems(new DateListedBirthdays(
                    item.get(0).getDay()+" "+months[Integer.parseInt(item.get(0).getMonth())-1]+",",
                    item));
        }

        mRecyclerView.setAdapter(customAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            //Toast.makeText(this,"Not implemented yet",Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(),SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





}
