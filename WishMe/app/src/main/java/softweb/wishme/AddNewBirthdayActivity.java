package softweb.wishme;


import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.codetroopers.betterpickers.datepicker.DatePickerBuilder;
import com.codetroopers.betterpickers.datepicker.DatePickerDialogFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import database.Birthdays;
import database.Singleton;
import dmax.dialog.SpotsDialog;
import interfaces.Strategy;

public class AddNewBirthdayActivity extends AppCompatActivity
        implements DatePickerDialogFragment.DatePickerDialogHandler, Strategy
{
    Singleton s = Singleton.getInstance();
    String[] relationNames={
            "Friend",
            "Family",
            "Co-workers",
            "Acquaintance",
            "Others"
    };

    Typeface digital,bold,thin;
    Birthdays birthdays;

    //views
    EditText etDatePicker,etName,etPhone;
    Button addBirthday;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_birthday);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        setSupportActionBar(toolbar);

        /** Working code */
        init();
        /* end working code */

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back);
    }

    public void init()
    {
        digital = s.getFontFromRes(getApplicationContext(),R.raw.digital);
        bold = s.getFontFromRes(getApplicationContext(),R.raw.rsbold);
        thin = s.getFontFromRes(getApplicationContext(),R.raw.rsthin);
        birthdays = new Birthdays();
        //
        etName  = (EditText) findViewById(R.id.name);
        etPhone = (EditText) findViewById(R.id.phone);
        //
        etDatePicker = (EditText) findViewById(R.id.date);
        etDatePicker.setTypeface(bold);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd : MM : yyyy");
        etDatePicker.setText( dateFormat.format(calendar.getTime()) );
        //
        RadioGroup radioGroup = (RadioGroup)findViewById(R.id.radio_group_gender);
        RadioButton radioMale = (RadioButton)findViewById(R.id.radio_male);
        RadioButton radioFemale = (RadioButton)findViewById(R.id.radio_female);
        radioMale.setTypeface(thin);
        radioFemale.setTypeface(thin);
        radioGroup.setOnCheckedChangeListener(radioGroupOnCheckedChangedListener);
        //
        Spinner spin = (Spinner) findViewById(R.id.spinner_relation);
        spin.setOnItemSelectedListener(spinOnItemsSelectedListener);
        CustomAdapter customAdapter=new CustomAdapter(getApplicationContext(),relationNames);
        spin.setAdapter(customAdapter);
        //
        addBirthday = (Button)findViewById(R.id.add_button);
        addBirthday.setOnClickListener(addBirthdayButtonOnClickListener);



    }

    RadioGroup.OnCheckedChangeListener radioGroupOnCheckedChangedListener = new RadioGroup.OnCheckedChangeListener()
    {

        /*
         * http://www.limbaniandroid.com/2014/05/custom-radio-buttons-example-in-android.html
         */
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            switch (i)
            {
                case R.id.radio_male:
                    //Toast.makeText(getApplicationContext(),"male",Toast.LENGTH_SHORT).show();
                    birthdays.setGender("male");
                    break;
                case R.id.radio_female:
                    //Toast.makeText(getApplicationContext(),"female",Toast.LENGTH_SHORT).show();
                    birthdays.setGender("female");
                    break;

                default:
                        break;
            }
        }
    };


    public void showDatePickerDialog(View v)
    {
        /*
         *  Date Picker help reference site: @Link(https://github.com/code-troopers/android-betterpickers)
         */
        DatePickerBuilder dpb = new DatePickerBuilder()
                .setFragmentManager(getSupportFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment)
                .setYearOptional(false);
        dpb.show();
    }

    @Override
    public void onDialogDateSet(int reference, int year, int monthOfYear, int dayOfMonth)
    {
        etDatePicker.setText(String.format("%02d", dayOfMonth) + " : "
                + String.format("%02d", (monthOfYear+1))  + " : "
                + String.format("%04d", year));

        birthdays.setDay(dayOfMonth+"");
        birthdays.setMonth((monthOfYear+1)+"");
        birthdays.setYear(year+"");
    }


    // spinner : relation combo box : http://abhiandroid.com/ui/custom-spinner-examples.html

    AdapterView.OnItemSelectedListener spinOnItemsSelectedListener = new AdapterView.OnItemSelectedListener()
    {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
        {
            //Toast.makeText(getApplicationContext(), relationNames[i], Toast.LENGTH_LONG).show();
            birthdays.setRelaion(relationNames[i]);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };
    public class CustomAdapter extends BaseAdapter
    {
        Context context;
        String[] relationNames;
        LayoutInflater inflter;

        public CustomAdapter(Context applicationContext, String[] relationNames) {
            this.context = applicationContext;
            this.relationNames = relationNames;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return relationNames.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            view = inflter.inflate(R.layout.spinner_layout, null);
            //ImageView icon = (ImageView) view.findViewById(R.id.imageView);
            TextView names = (TextView) view.findViewById(R.id.spinner_itm);
            names.setTypeface(thin);
            //icon.setImageResource(flags[i]);
            names.setText(relationNames[i]);
            return view;
        }
    }

    ///Button Add
    /// http://sourcey.com/beautiful-android-login-and-signup-screens-with-material-design/
    View.OnClickListener addBirthdayButtonOnClickListener= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            addNewBirthday();
        }
    };

    public boolean validate()
    {
        boolean valid = true;

        String tName  = etName.getText().toString();
        String tPhone = etPhone.getText().toString();

        if (tName.isEmpty() )
        {
            etName.setError("name field required *");
            valid = false;
        }
        else
        {
            birthdays.setName(tName);
            etName.setError(null);
        }

        if (tPhone.isEmpty() || tPhone.length() != 11)
        {
            etPhone.setError("must be 11 digit *");
            valid = false;
        }
        else
        {
            birthdays.setPhone(tPhone);
            etPhone.setError(null);
        }


        return valid;
    }

    private void onInsertSuccess()
    {
        s.insertAbirthday(this, birthdays.getName(),
                                birthdays.getDay(),
                                birthdays.getMonth(),
                                birthdays.getYear(),
                                birthdays.getRelaion(),
                                birthdays.getGender(),
                                birthdays.getPhone()
        );
    }



    public void addNewBirthday()
    {

        if (!validate())
        {
            //onLoginFailed();
            return;
        }

        addBirthday.setEnabled(false);

        /*final ProgressDialog progressDialog = new ProgressDialog(AddNewBirthdayActivity.this, R.style.CustomDialog);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Inserting...");
        progressDialog.show();*/

        /* SPOT DIALOG HELP FROM THESE SITES
         * https://github.com/d-max/spots-dialog/releases/tag/v0.7
         * https://github.com/d-max/spots-dialog/issues/10#issuecomment-150593143
         */
        final AlertDialog progressDialog = new SpotsDialog(AddNewBirthdayActivity.this, R.style.CustomDialog);
        progressDialog.show();



        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
            new Runnable()
            {
                public void run()
                {
                    // On complete call either onLoginSuccess or onLoginFailed
                    onInsertSuccess();
                    // onLoginFailed();
                    progressDialog.dismiss();
                }


            }, 3000);

        addBirthday.setEnabled(true);


    }



}
